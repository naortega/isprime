/*
 * Copyright (C) 2020  Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

int main(int argc, char *argv[]) {
	if(argc < 2)
	{
		fprintf(stderr, "Insufficient number of arguments. Use '-h' for more options.\n");
		return 2;
	}
	else if(argc > 2)
	{
		fprintf(stderr, "Too many arguments. Use '-h' for more options.\n");
		return 2;
	}
	if(strcmp(argv[1], "-h") == 0)
	{
		printf("USAGE: %s <number> | -h\n\n", argv[0]);
		puts("Calculates to see if a given number is prime, and returns success or\nfailure return code.\n");
		puts("OPTIONS:");
		puts("  <number>  number to test");
		puts("  -h        show this help information");
		return 0;
	}
	char *end;
	unsigned long long int test_num =
		strtoll(argv[1], &end, 10);
	if(test_num % 2 == 0)
		return 1;
	unsigned long long int test_root = sqrt(test_num);

	int not_prime = 0;

	#pragma omp parallel for
	for(unsigned long long int i = 3; i < test_root; i += 2)
	{
		if(test_num % i == 0)
			not_prime = 1;
	}

	return not_prime;
}
